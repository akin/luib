# https://trenki2.github.io/blog/2017/06/02/using-sdl2-with-cmake/
# absolute paths cannot be abused as easily as relative paths.
get_filename_component(CURRENT_DIR ${CMAKE_CURRENT_LIST_DIR} ABSOLUTE)
set(SDL2_INCLUDE_DIRS "${CURRENT_DIR}/include")

# Support both 32 and 64 bit builds
if (${CMAKE_SIZEOF_VOID_P} MATCHES 8)
  set(SDL2_LIBRARIES "${CURRENT_DIR}/lib/x64/SDL2.lib;${CURRENT_DIR}/lib/x64/SDL2main.lib")
  list(APPEND PROJECT_DLL_LIST "${CURRENT_DIR}/lib/x64/SDL2.dll")
else ()
  set(SDL2_LIBRARIES "${CURRENT_DIR}/lib/x86/SDL2.lib;${CURRENT_DIR}/lib/x86/SDL2main.lib")
  list(APPEND PROJECT_DLL_LIST "${CURRENT_DIR}/lib/x86/SDL2.dll")
endif ()

string(STRIP "${SDL2_LIBRARIES}" SDL2_LIBRARIES)