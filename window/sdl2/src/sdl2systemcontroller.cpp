#include <window/sdl2systemcontroller.h>
#include "SDL.h"
#include "SDL_syswm.h"

#include <window/sdl2keyboard.h>
#include <window/sdl2mouse.h>
#include <window/sdl2window.h>

#include <cassert>

namespace Lor {

SDL2SystemController::SDL2SystemController()
{
}

SDL2SystemController::~SDL2SystemController()
{
	quit();
}

bool SDL2SystemController::init(SystemController::Settings settings)
{
	this->settings = settings;
	if (!SDL_WasInit(SDL_INIT_VIDEO))
	{
		SDL_Init(SDL_INIT_VIDEO);
	}

	return true;
}

void SDL2SystemController::quit()
{
	SDL_Quit();
}

void SDL2SystemController::update()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		auto iter = handlers.find(event.type);
		if (iter == handlers.end())
		{
			// not found
			continue;
		}

		for (auto *it : iter->second)
		{
			it->handle(event);
		}
		/*
		switch (event.type)
		{
		case SDL_MOUSEMOTION:
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEWHEEL:
		// SDL_WindowEvent
		case SDL_WINDOWEVENT:
		// SDL_JoyAxisEvent
		case SDL_JOYAXISMOTION:
			// SDL_JoyBallEvent
		case SDL_JOYBALLMOTION:
			// SDL_JoyHatEvent
		case SDL_JOYHATMOTION:
			// SDL_JoyButtonEvent
		case SDL_JOYBUTTONDOWN:
		case SDL_JOYBUTTONUP:
			// SDL_JoyDeviceEvent
		case SDL_JOYDEVICEADDED:
		case SDL_JOYDEVICEREMOVED:
			// SDL_ControllerAxisEvent
		case SDL_CONTROLLERAXISMOTION:
			// SDL_ControllerButtonEvent
		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
			// SDL_ControllerDeviceEvent
		case SDL_CONTROLLERDEVICEADDED:
		case SDL_CONTROLLERDEVICEREMOVED:
		case SDL_CONTROLLERDEVICEREMAPPED:
			// SDL_AudioDeviceEvent
		case SDL_AUDIODEVICEADDED:
		case SDL_AUDIODEVICEREMOVED:
			// SDL_QuitEvent
		case SDL_QUIT:
			// SDL_SysWMEvent
		case SDL_SYSWMEVENT:
			// SDL_TouchFingerEvent
		case SDL_FINGERMOTION:
		case SDL_FINGERDOWN:
		case SDL_FINGERUP:
			// SDL_MultiGestureEvent
		case SDL_MULTIGESTURE:
			// SDL_DollarGestureEvent
		case SDL_DOLLARGESTURE:
		case SDL_DOLLARRECORD:
			// SDL_DropEvent
		case SDL_DROPFILE:
		case SDL_DROPTEXT:
		case SDL_DROPBEGIN:
		case SDL_DROPCOMPLETE:
		default:
			// SDL_UserEvent probably
			break;
		}
		*/
	}
}

SDL2SystemController::Settings SDL2SystemController::getSettings()
{
	return settings;
}

void SDL2SystemController::addHandler(uint32_t type, SDLEventHandler *handler)
{
	handlers[type].push_back(handler);
}

void SDL2SystemController::removeHandler(uint32_t type, SDLEventHandler *handler)
{
	auto iter = handlers.find(type);
	if (iter == handlers.end())
	{
		// not found
		return;
	}

	auto& vec = iter->second;
	for (auto i = vec.begin(); i != vec.end(); ++i)
	{
		if ((*i) == handler)
		{
			vec.erase(i);
			return;
		}
	}
}
} // ns Lor


/*
just for reference:
int main(int argc, char *argv[]) {
  SDL_Window* window;
  SDL_SysWMinfo info;

  SDL_Init(0);

  window = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_HIDDEN);

  SDL_VERSION(&info.version); // initialize info structure with SDL version info 

  if(SDL_GetWindowWMInfo(window,&info)) { // the call returns true on success 
    // success 
    const char *subsystem = "an unknown system!";
    switch(info.subsystem) {
      case SDL_SYSWM_UNKNOWN:   break;
      case SDL_SYSWM_WINDOWS:   subsystem = "Microsoft Windows(TM)";  break;
      case SDL_SYSWM_X11:       subsystem = "X Window System";        break;
#if SDL_VERSION_ATLEAST(2, 0, 3)
      case SDL_SYSWM_WINRT:     subsystem = "WinRT";                  break;
#endif
      case SDL_SYSWM_DIRECTFB:  subsystem = "DirectFB";               break;
      case SDL_SYSWM_COCOA:     subsystem = "Apple OS X";             break;
      case SDL_SYSWM_UIKIT:     subsystem = "UIKit";                  break;
#if SDL_VERSION_ATLEAST(2, 0, 2)
      case SDL_SYSWM_WAYLAND:   subsystem = "Wayland";                break;
      case SDL_SYSWM_MIR:       subsystem = "Mir";                    break;
#endif
#if SDL_VERSION_ATLEAST(2, 0, 4)
      case SDL_SYSWM_ANDROID:   subsystem = "Android";                break;
#endif
#if SDL_VERSION_ATLEAST(2, 0, 5)
      case SDL_SYSWM_VIVANTE:   subsystem = "Vivante";                break;
#endif
    }

    SDL_Log("This program is running SDL version %d.%d.%d on %s",
        (int)info.version.major,
        (int)info.version.minor,
        (int)info.version.patch,
        subsystem);
  } else {
    // call failed 
    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Couldn't get window information: %s", SDL_GetError());
  }

  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
*/