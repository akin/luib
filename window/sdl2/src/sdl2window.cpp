#include <window/sdl2window.h>
#include <window/sdl2systemcontroller.h>
#include <graphics/graphics_api.h>
#include "SDL.h"
#ifdef API_GRAPHICS_VULKAN
 #include "SDL_vulkan.h" // SDL_Vulkan_GetInstanceExtensions for getting extensions..
#endif
#include "SDL_syswm.h"
#include <cassert>

#include <platform/log.h>

namespace Lor {

SDL2Window::SDL2Window(SDL2SystemController& system)
: system(system)
{
	system.addHandler(SDL_WINDOWEVENT, this);
}

SDL2Window::~SDL2Window()
{
	system.removeHandler(SDL_WINDOWEVENT, this);

	destroy();
}

void SDL2Window::setSize(int width, int height)
{
	settings.size = Lor::Vec2i(width, height);
}

void SDL2Window::setTitle(std::string title)
{
	settings.title = title;
}

int SDL2Window::getWidth() const
{
	return settings.size.x;
}

int SDL2Window::getHeight() const
{
	return settings.size.y;
}

std::string SDL2Window::getTitle() const
{
	return settings.title;
}

SurfaceHandle SDL2Window::getSurfaceHandle()
{
	assert(window != nullptr);
	SDL_SysWMinfo info;
	SDL_VERSION(&info.version);
	assert(SDL_GetWindowWMInfo(window, &info) == SDL_TRUE);

	// TODO!
	// All the permutations for different platforms, so _right_ surfacehandle is returned from SDL Struct.
#ifdef PLATFORM_WINDOWS
	return info.info.win.window;
#elif PLATFORM_MAC
	return info.info.cocoa.window;
#endif
}

/*
bool SDL2Window::getExtensions(std::vector<std::string>& result) const
{
	assert(window != nullptr);

#ifdef API_GRAPHICS_VULKAN
	// if sdl does not have SDL_WINDOW_VULKAN in window creation SDL_Vulkan_GetInstanceExtensions will return SDL_FALSE
	unsigned int count = 0;
	if (!SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr))
	{
		return false;
	}
	std::vector<const char*> extensions;
	extensions.resize(count);
	if (!SDL_Vulkan_GetInstanceExtensions(window, &count, extensions.data()))
	{
		return false;
	}

	result.resize(count);
	for (size_t i = 0; i < count; ++i) 
	{
		result[i] = extensions[i];
	}

	return true;
#endif

	return false;
}
*/

#ifdef API_GRAPHICS_VULKAN
void report(SDL_Window *window)
{
	unsigned int count = 0;
	if (!SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr))
	{
		return;
	}
	std::vector<const char*> extensions;
	extensions.resize(count);
	if (!SDL_Vulkan_GetInstanceExtensions(window, &count, extensions.data()))
	{
		return;
	}

	LOG_MESSAGE("Graphics Context {0} extensions set!", extensions.size());
	for (size_t i = 0; i < count; ++i)
	{
		LOG_MESSAGE("    {0}", extensions[i]);
	}
}
#endif

bool SDL2Window::init(Settings settings)
{
	this->settings = settings;

	uint32_t flags =
		settings.fullscreen ? SDL_WINDOW_FULLSCREEN : 0 |
		settings.fullscreenDesktop ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0 |
		settings.hidden ? SDL_WINDOW_HIDDEN : 0 |
		settings.borderless ? SDL_WINDOW_BORDERLESS : 0 |
		settings.resizable ? SDL_WINDOW_RESIZABLE : 0 |
		settings.minimized ? SDL_WINDOW_MINIMIZED : 0 |
		settings.maximized ? SDL_WINDOW_MAXIMIZED : 0 |
		settings.inputGrabbed ? SDL_WINDOW_INPUT_GRABBED : 0 |
		settings.highDPI ? SDL_WINDOW_ALLOW_HIGHDPI : 0;

#ifdef API_GRAPHICS_VULKAN
	flags |= SDL_WINDOW_VULKAN;
#endif

	window = SDL_CreateWindow(
		settings.title.c_str(),
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		settings.size.x,
		settings.size.y,
		flags
	);

#ifdef API_GRAPHICS_VULKAN
	//report(window);
#endif

	windowID = SDL_GetWindowID(window);

	// Apparently SDL only needs to give hwnd to vulkan, .. to link these 2 together.
	// https://gist.github.com/TheBuzzSaw/9f54493b83b27fe36818
	/*
	SDL_SysWMinfo info;
	SDL_VERSION(&info.version);
	assert(SDL_GetWindowWMInfo(window, &info) == SDL_TRUE);
	surface.setSize(width, height);
	surface.setContext(info.info.win.window);
	surface.init();
	*/

	/*
	VkWin32SurfaceCreateInfoKHR w32sci = {};
	w32sci.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	w32sci.pNext = NULL;
	w32sci.hinstance = GetModuleHandle(NULL);
	w32sci.hwnd = info.info.win.window;
	*/

	/*

	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);

	SDL_Delay(3000);
	*/
	return true;
}

void SDL2Window::destroy()
{
	SDL_DestroyWindow(window);
	window = nullptr;
}

Window::Settings SDL2Window::getSettings()
{
	return settings;
}

void SDL2Window::handle(const SDL_Event& event)
{
	if (event.type != SDL_WINDOWEVENT)
	{
		return;
	}

	// Resize & such events handled separetly.
	switch (event.window.event)
	{
		case SDL_WINDOWEVENT_RESIZED:
		case SDL_WINDOWEVENT_SIZE_CHANGED:
		{
			auto *listener = Property<ResizeListener>::get();
			if (listener == nullptr)
			{
				return;
			}

			ResizeEvent resize;
			resize.size.x = event.window.data1;
			resize.size.y = event.window.data2;

			listener->handle(resize);
			return;
		}
		default:
			break;
	}

	auto* listener = Property<WindowListener>::get();
	if (listener == nullptr)
	{
		return;
	}

	WindowEvent::Type wevent = WindowEvent::Type::none;
	switch (event.window.event)
	{
	case SDL_WINDOWEVENT_SHOWN:
		wevent = WindowEvent::Type::show;
		break;
	case SDL_WINDOWEVENT_HIDDEN:
		wevent = WindowEvent::Type::hide;
		break;
	case SDL_WINDOWEVENT_EXPOSED:
		break;
	case SDL_WINDOWEVENT_MOVED:
		wevent = WindowEvent::Type::move;
		//event.window->window.data1, event.window->window.data2);
		break;
	case SDL_WINDOWEVENT_MINIMIZED:
		wevent = WindowEvent::Type::minimize;
		break;
	case SDL_WINDOWEVENT_MAXIMIZED:
		wevent = WindowEvent::Type::maximize;
		break;
	case SDL_WINDOWEVENT_RESTORED:
		wevent = WindowEvent::Type::restore;
		break;
	case SDL_WINDOWEVENT_ENTER:
		wevent = WindowEvent::Type::mouse_enter;
		break;
	case SDL_WINDOWEVENT_LEAVE:
		wevent = WindowEvent::Type::mouse_leave;
		break;
	case SDL_WINDOWEVENT_FOCUS_GAINED:
		wevent = WindowEvent::Type::focus_gained;
		break;
	case SDL_WINDOWEVENT_FOCUS_LOST:
		wevent = WindowEvent::Type::focus_lost;
		break;
	case SDL_WINDOWEVENT_CLOSE:
		wevent = WindowEvent::Type::close;
		break;
		/*
		case SDL_WINDOWEVENT_TAKE_FOCUS:
		SDL_Log("Window %d is offered a focus", event->window.windowID);
		break;
		case SDL_WINDOWEVENT_HIT_TEST:
		SDL_Log("Window %d has a special hit test", event->window.windowID);
		break;
		*/
	default:
		break;
	}

	if (wevent != WindowEvent::Type::none)
	{
		WindowEvent woot;
		woot.type = wevent;

		listener->handle(woot);
		return;
	}
}

} // ns Lor
