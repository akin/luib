#include <window/sdl2mouse.h>
#include <window/sdl2systemcontroller.h>
#include <cassert>

namespace Lor {
SDL2Mouse::SDL2Mouse(SDL2SystemController& system)
: system(system)
{
	system.addHandler(SDL_MOUSEMOTION, this);
	system.addHandler(SDL_MOUSEBUTTONDOWN, this);
	system.addHandler(SDL_MOUSEBUTTONUP, this);
	system.addHandler(SDL_MOUSEWHEEL, this);
}

SDL2Mouse::~SDL2Mouse()
{
	system.removeHandler(SDL_MOUSEMOTION, this);
	system.removeHandler(SDL_MOUSEBUTTONDOWN, this);
	system.removeHandler(SDL_MOUSEBUTTONUP, this);
	system.removeHandler(SDL_MOUSEWHEEL, this);
}

void SDL2Mouse::handle(const SDL_Event& event)
{
	auto* listener = Property<MouseListener>::get();
	if (listener == nullptr)
	{
		return;
	}
	switch (event.type)
	{
	case SDL_MOUSEMOTION:
	{
		MouseMoveEvent move;
		move.id = event.motion.which;
		move.position.x = event.motion.x;
		move.position.y = event.motion.y;
		move.relative.x = event.motion.xrel;
		move.relative.y = event.motion.yrel;

		listener->handle(move);

		return;
	}
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
	{
		MouseClickEvent click;
		click.id = event.button.which;
		click.position.x = event.button.x;
		click.position.y = event.button.y;
		click.state = event.button.state == SDL_PRESSED ? ButtonState::down : ButtonState::up;
		switch (event.button.button)
		{
			case SDL_BUTTON_LEFT: click.button = Mouse::Button::left; break;
			case SDL_BUTTON_RIGHT: click.button = Mouse::Button::right; break;
			case SDL_BUTTON_MIDDLE: click.button = Mouse::Button::middle; break;
			case SDL_BUTTON_X1: click.button = Mouse::Button::extra1; break;
			case SDL_BUTTON_X2: click.button = Mouse::Button::extra2; break;
			default:
				break;
		}
		click.clicks = event.button.clicks;

		listener->handle(click);

		return;
	}
	case SDL_MOUSEWHEEL:
	{
		MouseWheelEvent wheel;
		wheel.id = event.wheel.which;
		wheel.position.x = event.wheel.x;
		wheel.position.y = event.wheel.y;

		listener->handle(wheel);

		return;
	}
	default:
		break;
	}
	return;
}
} // ns Lor
