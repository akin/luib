#include <window/sdl2keyboard.h>
#include <window/sdl2systemcontroller.h>
#include <cassert>

namespace Lor {

SDL2Keyboard::SDL2Keyboard(SDL2SystemController& system)
: system(system)
{
	system.addHandler(SDL_KEYDOWN, this);
	system.addHandler(SDL_KEYUP, this);
	system.addHandler(SDL_TEXTEDITING, this);
	system.addHandler(SDL_TEXTINPUT, this);
}

SDL2Keyboard::~SDL2Keyboard()
{
	system.removeHandler(SDL_KEYDOWN, this);
	system.removeHandler(SDL_KEYUP, this);
	system.removeHandler(SDL_TEXTEDITING, this);
	system.removeHandler(SDL_TEXTINPUT, this);
}

void SDL2Keyboard::handle(const SDL_Event& event)
{
	auto* listener = Property<KeyboardListener>::get();
	if (listener == nullptr)
	{
		return;
	}
	switch (event.type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
	{
		//SDL_KeyboardEvent aa;
		// Uint32 windowID
		// TODO, what is the keyboard focusing?
		KeyEvent key;
		key.code = static_cast<KeyCode>(event.key.keysym.sym);
		key.state = event.type == SDL_KEYDOWN ? ButtonState::down : ButtonState::up;

		listener->handle(key);

		return;
	}
	// SDL_TextEditingEvent
	case SDL_TEXTEDITING:
	// SDL_TextInputEvent
	case SDL_TEXTINPUT:
	default:
		break;
	}
}

std::string SDL2Keyboard::getKey(KeyCode code)
{
	return SDL_GetKeyName(static_cast<SDL_Keycode>(code));
}

KeyCode SDL2Keyboard::getKey(std::string code)
{
	return static_cast<KeyCode>(SDL_GetKeyFromName(code.c_str()));
}

} // ns Lor