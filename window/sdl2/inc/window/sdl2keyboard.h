#pragma once
#ifndef LOR_SDL2KEYBOARD_H_
#define LOR_SDL2KEYBOARD_H_

#include <interface/keyboard.h>
//#include "graphics_api.h"
#include "sdl2common.h"

namespace Lor {
class SDL2SystemController;
class SDL2Keyboard : public Keyboard, public SDLEventHandler
{
private:
	SDL2SystemController& system;
public:
	SDL2Keyboard(SDL2SystemController& system);
	virtual ~SDL2Keyboard();

	virtual std::string getKey(KeyCode code) override;
	virtual KeyCode getKey(std::string code) override;

	virtual void handle(const SDL_Event& event) override;
};
} // ns Lor

#endif // LOR_SDL2KEYBOARD_H_