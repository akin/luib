#pragma once
#ifndef LOR_SDL2COMMON_H_
#define LOR_SDL2COMMON_H_

#include <platform/common.h>
#include "SDL.h"
#include "SDL_syswm.h"

namespace Lor {

class SDLEventHandler
{
public:
	virtual ~SDLEventHandler() {}
	virtual void handle(const SDL_Event& event) = 0;
};

} // ns Lor

#endif // LOR_SDL2COMMON_H_