#pragma once
#ifndef LOR_SDL2MOUSE_H_
#define LOR_SDL2MOUSE_H_

#include <interface/mouse.h>
//#include "graphics_api.h"
#include "sdl2common.h"

namespace Lor {
class SDL2SystemController;
class SDL2Mouse : public Mouse, public SDLEventHandler
{
private:
	SDL2SystemController& system;
public:
	SDL2Mouse(SDL2SystemController& system);
	virtual ~SDL2Mouse();

	virtual void handle(const SDL_Event& event) override;
};
} // ns Lor

#endif // LOR_SDL2MOUSE_H_