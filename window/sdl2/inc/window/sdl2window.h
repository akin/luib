#pragma once
#ifndef LOR_SDL2WINDOW_H_
#define LOR_SDL2WINDOW_H_

#include <platform/common.h>
#include <interface/window.h>
#include "sdl2common.h"

namespace Lor {

class SDL2SystemController;
class SDL2Window : public Window, public SDLEventHandler
{
private:
	SDL2SystemController& system;
	uint32_t windowID = 0;
	SDL_Window *window = nullptr;

	Window::Settings settings;
public:
	SDL2Window(SDL2SystemController& system);
	virtual ~SDL2Window();

	virtual void setSize(int width, int height) override;
	virtual void setTitle(std::string title) override;

	virtual int getWidth() const override;
	virtual int getHeight() const override;

	virtual std::string getTitle() const override;

	virtual SurfaceHandle getSurfaceHandle() override;

	virtual bool init(Settings settings) override;
	virtual void destroy() override;
	
	virtual Settings getSettings() override;

	virtual void handle(const SDL_Event& event) override;
};
} // ns Lor

#endif // LOR_SDL2WINDOW_H_