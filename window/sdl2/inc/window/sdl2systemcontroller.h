#pragma once
#ifndef LOR_SDL2SYSTEM_CONTROLLER_H_
#define LOR_SDL2SYSTEM_CONTROLLER_H_

#include <platform/common.h>
#include <interface/systemcontroller.h>
#include <unordered_map>
#include <vector>
#include "sdl2common.h"

namespace Lor {

class SDL2SystemController : public SystemController
{
private:
	SystemController::Settings settings;
	std::unordered_map<uint32_t, std::vector<SDLEventHandler*>> handlers;
public:
	SDL2SystemController();
	virtual ~SDL2SystemController();

	virtual bool init(Settings settings) override;
	virtual void quit() override;
	virtual void update() override;
	
	virtual Settings getSettings() override;

	void addHandler(uint32_t type, SDLEventHandler *handler);
	void removeHandler(uint32_t type, SDLEventHandler *handler);
};
} // ns Lor

#endif // LOR_SDL2SYSTEM_CONTROLLER_H_