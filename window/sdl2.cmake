cmake_minimum_required(VERSION 3.7)

# include all window things
include_directories("${CMAKE_CURRENT_LIST_DIR}/sdl2/inc")
set(SDL2_DIR "${CMAKE_CURRENT_LIST_DIR}/libs/sdl2")

find_package(SDL2 REQUIRED)
list(APPEND LIBRARY_LIBS ${SDL2_LIBRARIES})
include_directories(${SDL2_INCLUDE_DIRS})

## Basic library packing
include_directories("${CMAKE_CURRENT_LIST_DIR}/sdl2/inc")
list(APPEND LIBRARY_SOURCES 
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/inc/window/sdl2common.h"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/inc/window/sdl2window.h"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/src/sdl2window.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/inc/window/sdl2mouse.h"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/src/sdl2mouse.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/inc/window/sdl2keyboard.h"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/src/sdl2keyboard.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/inc/window/sdl2systemcontroller.h"
	"${CMAKE_CURRENT_LIST_DIR}/sdl2/src/sdl2systemcontroller.cpp"
)