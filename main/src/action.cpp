#include "action.h"
#include <cassert>

Action::Action(std::string name, std::string description)
: name(name)
, description(description)
{
}

Action::Action(std::string name, std::string description, std::function<void(ActionState)> callback)
: name(name)
, description(description)
, callback(callback)
{
}

Action::~Action()
{
}

void Action::setDescription(std::string description)
{
	this->description = description;
}

std::string Action::getDescription() const
{
	return description;
}

std::string Action::getName() const
{
	return name;
}

void Action::activate(ActionState state)
{
	if (callback)
	{
		callback(state);
	}
}

void Action::set(std::function<void(ActionState)> callback)
{
	this->callback = callback;
}
