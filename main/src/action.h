#pragma once
#ifndef ACTION_H_
#define ACTION_H_

#include <interface/keyboard.h>
#include <interface/mouse.h>
#include <json/json.hpp>
#include <unordered_map>

enum class ActionState : uint8_t {
	none = 0,
	begin,
	update,
	done,
	cancel
};

class Action
{
private:
	const std::string name;
	std::string description;
	std::function<void(ActionState)> callback;
public:
	Action(const Action& other) = delete;

	Action(std::string name, std::string description);
	Action(std::string name, std::string description, std::function<void(ActionState)> callback);
	~Action();

	void setDescription(std::string description);
	std::string getDescription() const;

	std::string getName() const;

	void activate(ActionState state = ActionState::done);

	void set(std::function<void(ActionState)> callback);
};

#endif // ACTION_H_