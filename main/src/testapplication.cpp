#include "testapplication.h"
#include <iostream>
#include <fstream>
#include <platform/log.h>
#include <thread>
#include <chrono>

#include <window/sdl2systemcontroller.h>
#include <window/sdl2window.h>
#include <window/sdl2keyboard.h>
#include <window/sdl2mouse.h>

TestApplication::TestApplication()
: exitAction{ "quit", "quit the application" , [this](ActionState state) { quit(true); } }
, exitActivation{ "Escape", &exitAction }
{
	std::shared_ptr<Lor::SDL2SystemController> sdlsystem = std::make_shared<Lor::SDL2SystemController>();

	window = std::make_shared<Lor::SDL2Window>(*sdlsystem);
	keyboard = std::make_shared<Lor::SDL2Keyboard>(*sdlsystem);
	mouse = std::make_shared<Lor::SDL2Mouse>(*sdlsystem);
	system = std::static_pointer_cast<Lor::SystemController>(sdlsystem);

	inputController.set(keyboard);
	inputController.set(mouse);

	exitActivation.attach(inputController);
}

TestApplication::~TestApplication()
{
	clean();
}

bool TestApplication::readConfiguration(std::string path)
{
	std::ifstream input;
	input.open(path, std::ifstream::in);

	if ((!input.is_open()) || (!input.good())) {
		LOG_ERROR("Failed to open file '{0}'.", path);
		return false;
	}

	input >> config;

	return true;
}

bool TestApplication::init()
{
	Lor::Window::Settings settings;
	settings.size = { 800, 600 };
	settings.borderless = false;
	settings.highDPI = true;
	settings.resizable = true;
	if (!window->init(settings))
	{
		return false;
	}

	context = std::make_unique<Lor::Graphics::Context>();
	context->setExtensions(extensions.get(GraphicsModule::instance));
	if (!context->init())
	{
		return false;
	}

	surface = context->createSurface();
	surface->setContext(window->getSurfaceHandle());
	if (!surface->init())
	{
		return false;
	}

	running = true;
	/*
	mouse->setHandler([this](const Lor::MouseMoveEvent& move) {
		LOG_MESSAGE("Mouse {0} to x: {1} y: {2}.",
			move.id,
			move.position.x,
			move.position.y
		);
	});

	mouse->setHandler([this](const Lor::MouseWheelEvent& wheel) {
		LOG_MESSAGE("Mouse {0} wheel  to x: {1} y: {2}.",
			wheel.id,
			wheel.position.x,
			wheel.position.y
		);
	});

	mouse->setHandler([this](const Lor::MouseClickEvent& click) {
		LOG_MESSAGE("Mouse {0} click {3} to x: {1} y: {2}.",
			click.id,
			click.position.x,
			click.position.y,
			static_cast<uint32_t>(click.button)
		);
	});

	surface.setHandler([this](const Lor::SurfaceResizeEvent& event) {
		LOG_MESSAGE("Window resize to x: {0} y: {1}.",
			event.size.x,
			event.size.y
		);
	});

	window->setHandler([this](const Lor::WindowEvent& event) {
		if (event.type == Lor::WindowEvent::Type::close)
		{
			quit(true);
		}
	});
	*/
	return true;
}

int TestApplication::run()
{
	while (running)
	{
		// Run system update event polling
		system->update();

		// Logic

		// Render

		// Sleep
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	return exitStatus;
}

void TestApplication::clean()
{
	system->quit();
}

void TestApplication::quit(bool success)
{
	LOG_MESSAGE("Quit requested.");

	running = false;
	exitStatus = success ? EXIT_SUCCESS : EXIT_FAILURE;
}