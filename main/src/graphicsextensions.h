#pragma once
#ifndef GRAPHICSEXTENSIONS_H_
#define GRAPHICSEXTENSIONS_H_

#include <vector>
#include <unordered_map>
#include <string>

enum class GraphicsModule
{
	none = 0,
	instance = 1
};

class GraphicsExtensions
{
private:
	std::unordered_map<GraphicsModule, std::vector<std::string>> extensions;
	std::vector<std::string> empty;
public:
	GraphicsExtensions();
	~GraphicsExtensions();

	const std::vector<std::string>& get(GraphicsModule module) const;
};

#endif // GRAPHICSEXTENSIONS_H_