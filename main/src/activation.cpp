#include "activation.h"
#include "inputcontroller.h"
#include "action.h"
#include <cassert>


Activation::Activation(Action *action)
: action(action)
{
}

Activation::~Activation()
{
}

void Activation::set(Action *action)
{
	this->action = action;
}

KeyboardActivation::KeyboardActivation(std::string key, Action *action)
: Activation(action)
, key(key)
{
	if (controller != nullptr)
	{
		attach(*controller);
	}
}

KeyboardActivation::~KeyboardActivation()
{
	detach();
}

void KeyboardActivation::attach(InputController& controller)
{
	this->controller = &controller;

	// Register this to receive keyboard events.. handling them etc.. though.. All sorts of lamda interface overload shitfest is going on here now.. 
	code = controller.getKey(key);

	controller.addListener(code, this);
}

void KeyboardActivation::detach()
{
	if (this->controller != nullptr)
	{
		this->controller->removeListener(code, this);
		this->controller = nullptr;
	}
}

void KeyboardActivation::setActive(bool active)
{
}

bool KeyboardActivation::isActive() const
{
	return true;
}

bool KeyboardActivation::handle(const Lor::KeyEvent& event)
{
	if (event.code == code && event.state == Lor::ButtonState::down)
	{
		if (action != nullptr)
		{
			action->activate();
		}
		return true;
	}
	return false;
}

