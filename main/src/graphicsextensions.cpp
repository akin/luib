#include "graphicsextensions.h"
#include <cassert>

void addExtensions(std::unordered_map<GraphicsModule, std::vector<std::string>>& map);

GraphicsExtensions::GraphicsExtensions()
{
	addExtensions(extensions);
}

GraphicsExtensions::~GraphicsExtensions()
{
}

#ifdef API_GRAPHICS_VULKAN
void addExtensions(std::unordered_map<GraphicsModule, std::vector<std::string>>& map)
{
	// Instace
	{
		std::vector<std::string> extensions;
		extensions.push_back("VK_KHR_surface");
#ifdef PLATFORM_WINDOWS
		extensions.push_back("VK_KHR_win32_surface");
#endif

		map[GraphicsModule::instance] = extensions;
	}
}
#endif

const std::vector<std::string>& GraphicsExtensions::get(GraphicsModule module) const
{
	auto iter = extensions.find(module);

	if (iter != extensions.end())
	{
		return iter->second;
	}

	return empty;
}

