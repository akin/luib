#pragma once
#ifndef INPUTCONTROLLER_H_
#define INPUTCONTROLLER_H_

#include <interface/keyboard.h>
#include <interface/mouse.h>
#include <json/json.hpp>
#include <unordered_map>
#include <vector>
#include <memory>

class InputController : public Lor::KeyboardListener, public Lor::MouseListener
{
private:
	std::unordered_map<Lor::KeyCode, std::vector<Lor::KeyboardListener*>> keyboardListeners;
	std::vector<Lor::MouseListener*> mouseListeners;
private:
	std::shared_ptr<Lor::Keyboard> keyboard;
	std::shared_ptr<Lor::Mouse> mouse;

	void clear();
public:
	InputController();
	~InputController();

	void set(const std::shared_ptr<Lor::Keyboard>& keyboard);
	void set(const std::shared_ptr<Lor::Mouse>& mouse);

	// invent more elegant way.. 
	Lor::KeyCode getKey(std::string code);

	void addListener(Lor::KeyCode code, Lor::KeyboardListener* listener);
	void addListener(Lor::MouseListener* listener);

	void removeListener(Lor::KeyCode code, Lor::KeyboardListener* listener);
	void removeListener(Lor::MouseListener* listener);

	// Lor::Keyboard::Listener
	virtual bool handle(const Lor::KeyEvent& event) override;

	// Lor::Mouse::Listener
	virtual bool handle(const Lor::MouseMoveEvent& event) override;
	virtual bool handle(const Lor::MouseClickEvent& event) override;
	virtual bool handle(const Lor::MouseWheelEvent& event) override;
};

#endif // INPUTCONTROLLER_H_