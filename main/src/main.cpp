#include <iostream>
#include <testapplication.h>
#include <string>
#include <platform/log.h>

#ifndef APPLICATION_CONFIG_FILE
	#define APPLICATION_CONFIG_FILE "config.json"
#endif

int main(int argc, char *argv[])
{
	TestApplication app;
	std::string configFile = APPLICATION_CONFIG_FILE;
	if (argc > 1)
	{
		configFile = argv[1];
	}
	if (!app.readConfiguration(configFile)) {
		LOG_ERROR("Failed to read configuration file '{0}'.", configFile);
		return EXIT_FAILURE;
	}

	int ret = EXIT_FAILURE;
	if (app.init()) {
		ret = app.run();
	}
	app.clean();

	return ret;
}
