#include "inputcontroller.h"
#include "activation.h"
#include <cassert>

InputController::InputController()
{
}

InputController::~InputController()
{
	clear();
}

Lor::KeyCode InputController::getKey(std::string code)
{
	// You haven't setup the keyboard for this yet, maybe you should do that, before asking key translations.
	assert(keyboard != nullptr);
	return keyboard->getKey(code);
}

void InputController::set(const std::shared_ptr<Lor::Keyboard>& keyboard)
{
	this->keyboard = keyboard;
	keyboard->Property<Lor::KeyboardListener>::set(this);
}

void InputController::set(const std::shared_ptr<Lor::Mouse>& mouse)
{
	this->mouse = mouse;
	mouse->Property<Lor::MouseListener>::set(this);
}

void InputController::clear()
{
	mouse->Property<Lor::MouseListener>::set(nullptr);
	keyboard->Property<Lor::KeyboardListener>::set(nullptr);

	mouse.reset();
	keyboard.reset();
}

void InputController::addListener(Lor::KeyCode code, Lor::KeyboardListener* listener)
{
	keyboardListeners[code].push_back(listener);
}

void InputController::addListener(Lor::MouseListener* listener)
{
	mouseListeners.push_back(listener);
}

void InputController::removeListener(Lor::KeyCode code, Lor::KeyboardListener* listener)
{
	auto iter = keyboardListeners.find(code);
	if (iter == keyboardListeners.end())
	{
		return;
	}

	auto& vec = iter->second;
	for (auto i = vec.begin(); i != vec.end(); ++i)
	{
		if ((*i) == listener)
		{
			vec.erase(i);
			return;
		}
	}
}

void InputController::removeListener(Lor::MouseListener* listener)
{
	for (auto i = mouseListeners.begin(); i != mouseListeners.end(); ++i)
	{
		if ((*i) == listener)
		{
			mouseListeners.erase(i);
			return;
		}
	}
}

// Lor::KeyboardListener
bool InputController::handle(const Lor::KeyEvent& event)
{
	auto iter = keyboardListeners.find(event.code);
	if (iter != keyboardListeners.end())
	{
		for (auto* listener : iter->second)
		{
			if (listener->handle(event))
			{
				break;
			}
		}
	}
	return true;
}

// Lor::MouseListener
bool InputController::handle(const Lor::MouseMoveEvent& event)
{
	for (auto* listener : mouseListeners)
	{
		if (listener->handle(event))
		{
			break;
		}
	}
	return true;
}

bool InputController::handle(const Lor::MouseClickEvent& event)
{
	for (auto* listener : mouseListeners)
	{
		if (listener->handle(event))
		{
			break;
		}
	}
	return true;
}

bool InputController::handle(const Lor::MouseWheelEvent& event)
{
	for (auto* listener : mouseListeners)
	{
		if (listener->handle(event))
		{
			break;
		}
	}
	return true;
}