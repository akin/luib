#pragma once
#ifndef ACTIVATION_H_
#define ACTIVATION_H_

#include <interface/keyboard.h>
#include <interface/mouse.h>
#include <json/json.hpp>
#include <unordered_map>

// Does not know about controller.
class Action;
class InputController;
class Activation
{
protected:
	Action *action = nullptr;
public:
	Activation(Action *action = nullptr);
	virtual ~Activation();

	void set(Action *action);

	virtual void setActive(bool active) = 0;
	virtual bool isActive() const = 0;
};

class KeyboardActivation : public Activation, public Lor::KeyboardListener
{
private:
	std::string key;
	Lor::KeyCode code;

	InputController *controller = nullptr;
public:
	KeyboardActivation(std::string key, Action *action = nullptr);
	virtual ~KeyboardActivation();

	void attach(InputController& controller);
	void detach();

	virtual void setActive(bool active) override;
	virtual bool isActive() const override;

	virtual bool handle(const Lor::KeyEvent& event) override;
};

#endif // ACTIVATION_H_