#pragma once
#ifndef LUIB_APPLICATION_H_
#define LUIB_APPLICATION_H_

#include <core/common.h>
#include <core/application.h>
#include <core/config.h>
#include <interface/window.h>
#include <interface/systemcontroller.h>
#include <interface/keyboard.h>
#include <interface/mouse.h>
#include "action.h"
#include "activation.h"
#include "inputcontroller.h"

#include "graphicsextensions.h"

#include <graphics/context.h>
#include <graphics/surface.h>

class TestApplication : public Lor::Application
{
private:
	std::shared_ptr<Lor::SystemController> system;
	std::shared_ptr<Lor::Window> window;
	std::shared_ptr<Lor::Keyboard> keyboard;
	std::shared_ptr<Lor::Mouse> mouse;

	std::unique_ptr<Lor::Graphics::Context> context;
	std::shared_ptr<Lor::Graphics::Surface> surface;

	Lor::Config config;
	bool running;
	int exitStatus;

	InputController inputController;

	Action exitAction;
	KeyboardActivation exitActivation;
	GraphicsExtensions extensions;
public:
	TestApplication();
	virtual ~TestApplication();

	bool readConfiguration(std::string path);

	virtual bool init() override;
	virtual int run() override;
	virtual void clean() override;

	virtual void quit(bool success) override;
};

#endif // LUIB_APPLICATION_H_