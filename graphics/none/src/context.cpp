#include <graphics/context.h>
#include <cassert>
#include "graphics_api.h"

namespace Lor {
namespace Graphics {

class ContextImpl
{
private:
	Context *parent = nullptr;
public:
	ContextImpl(Context *parent)
	: parent(parent)
	{
	}

	void init()
	{
	}
};

Context::Context()
: impl(new ContextImpl(this))
{
}

Context::~Context()
{
	delete impl;
	impl = nullptr;
}
} // ns Graphics
} // ns Lor