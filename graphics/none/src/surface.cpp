#include <graphics/surface.h>
#include <cassert>
#include <iostream>
#include "graphics_api.h"

namespace Lor {
namespace Graphics {

class SurfaceImpl
{
private:
	Surface *parent = nullptr;
	int width;
	int height;
public:
	SurfaceImpl(Surface *parent)
	: parent(parent)
	{
	}

	void setContext(SurfaceHandle handle)
	{
		this->window = window;
	}

	void setSize(int width, int height)
	{
		this->width = width;
		this->height = height;
	}

	void resize(int width, int height)
	{
		this->width = width;
		this->height = height;
	}

	void report()
	{
	}

	bool init()
	{
		return true;
	}
};

Surface::Surface()
: impl(new SurfaceImpl(this))
{
}

Surface::~Surface()
{
	delete impl;
	impl = nullptr;
}

bool Surface::init()
{
	impl->report();
	return impl->init();
}

Surface& Surface::setContext(SurfaceHandle handle)
{
	impl->setContext(handle);
	return *this;
}

Surface& Surface::setSize(int width, int height)
{
	impl->setSize(width, height);
	return *this;
}

void Surface::doResize(int width, int height)
{
	impl->resize(width, height);
}
} // ns Graphics
} // ns Lor
