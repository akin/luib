cmake_minimum_required(VERSION 3.7)

set(LIBRARY_PATH "${CMAKE_CURRENT_LIST_DIR}/graphics")
include_directories("${LIBRARY_PATH}/inc")

#list(APPEND GRAPHICS_SOURCES 
#    "${LIBRARY_PATH}/inc/graphics/context.h"
#    "${LIBRARY_PATH}/inc/graphics/surface.h"
#)
