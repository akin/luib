cmake_minimum_required(VERSION 3.7)

find_package(Vulkan)
if(NOT Vulkan_FOUND)
    message(FATAL_ERROR "Could not find Vulkan library!")
endif()
include_directories("${Vulkan_INCLUDE_DIRS}")

set(LIBRARY_PATH "${CMAKE_CURRENT_LIST_DIR}/vulkan")
include_directories("${LIBRARY_PATH}/inc")

list(APPEND GRAPHICS_SOURCES 
    "${LIBRARY_PATH}/inc/graphics/graphics_api.h"
    "${LIBRARY_PATH}/inc/graphics/context.h"
    "${LIBRARY_PATH}/inc/graphics/surface.h"
    "${LIBRARY_PATH}/src/context.cpp"
    "${LIBRARY_PATH}/src/surface.cpp"
)

list(APPEND GRAPHICS_LIBRARIES
    ${Vulkan_LIBRARY}
)
