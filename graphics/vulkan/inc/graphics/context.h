#pragma once
#ifndef GRAPHICS_CONTEXT_H_
#define GRAPHICS_CONTEXT_H_

#include "graphics_api.h"
#include <memory>
#include <vector>
#include <string>

namespace Lor {
namespace Graphics {

class Surface;
class Context
{
private:
	VkInstance instance;
	VkApplicationInfo appInfo = {};
	VkInstanceCreateInfo createInfo = {};

	std::vector<std::string> extensions;
public:
	Context();
	~Context();

	void setExtensions(const std::vector<std::string>& extensions);
	
	bool init();

	std::shared_ptr<Lor::Graphics::Surface> createSurface();

	friend class Surface;
};
} // ns Graphics
} // ns Lor

#endif // GRAPHICS_CONTEXT_H_