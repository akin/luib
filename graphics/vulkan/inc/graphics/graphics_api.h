#pragma once
#ifndef LOR_GRAPHICS_API_HEADER
#define LOR_GRAPHICS_API_HEADER
/*
 * Oddball file to include all graphics api specifics
 */

// Well, wherever we are including things, there should be PLATFORM defines in place
// and defining these in CMAKE would be messy..
// This header is _always_ for vulkan.
#ifdef PLATFORM_WINDOWS
 // win32?? wtf.. is there win64 also defined?? or have khronos taken just some pills in crusade for future proofing..
 #define VK_USE_PLATFORM_WIN32_KHR
#endif

#include <vulkan/vulkan.h>

#endif // LOR_GRAPHICS_API_HEADER