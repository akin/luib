#pragma once
#ifndef GRAPHICS_SURFACE_H_
#define GRAPHICS_SURFACE_H_

#include <platform/common.h>
#include "graphics_api.h"

namespace Lor {
namespace Graphics {
class Context;
class Surface
{
private:
	SurfaceHandle handle = nullptr;
	int width;
	int height;
	Context& context;

	//VkWin32SurfaceCreateInfoKHR
public:
	Surface(Context& context);
	~Surface();

	Surface& setContext(SurfaceHandle handle);
	Surface& setSize(int width, int height);
	bool init();
	
	void report();

	void doResize(int width, int height);
};
} // ns Graphics
} // ns Lor

#endif // SURFACE_H_