
#include <graphics/graphics_api.h>
#include <graphics/surface.h>
#include <iostream>
#include <cassert>

namespace Lor {
namespace Graphics {

Surface::Surface(Context& context)
: context(context)
{
}

Surface::~Surface()
{
}

void Surface::report()
{
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::cout << extensionCount << " extensions supported" << std::endl;
}

bool Surface::init()
{
	report();
	
	VkWin32SurfaceCreateInfoKHR w32sci = {};
	w32sci.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	w32sci.pNext = NULL;
	w32sci.hinstance = GetModuleHandle(NULL);
	w32sci.hwnd = handle;
	
	return true;
}

Surface& Surface::setContext(SurfaceHandle handle)
{
	this->handle = handle;
	return *this;
}

Surface& Surface::setSize(int width, int height)
{
	this->width = width;
	this->height = height;
	return *this;
}

void Surface::doResize(int width, int height)
{
	this->width = width;
	this->height = height;
}
} // ns Graphics
} // ns Lor
