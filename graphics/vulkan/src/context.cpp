#include <graphics/context.h>
#include <graphics/surface.h>
#include <cassert>

#include <platform/log.h>

namespace Lor {
namespace Graphics {

Context::Context()
{
}

Context::~Context()
{
}

void Context::setExtensions(const std::vector<std::string>& extensions)
{
	this->extensions = extensions;
}

bool Context::init()
{
	VkResult result;

	// Optional info.. shrug..
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = APPLICATION_NAME " - " APPLICATION_VERSION;
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = ENGINE_NAME " - " ENGINE_VERSION;
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	std::vector<const char*> names;
	names.resize(extensions.size());
	for (size_t i = 0; i < extensions.size(); ++i)
	{
		names[i] = extensions[i].c_str();
	}

	createInfo.enabledExtensionCount = static_cast<uint32_t>(names.size());
	createInfo.ppEnabledExtensionNames = names.data();

	createInfo.enabledLayerCount = 0;

	result = vkCreateInstance(&createInfo, nullptr, &instance);
	if (result != VK_SUCCESS)
	{
		return false;
	}

	return true;
}

 std::shared_ptr<Lor::Graphics::Surface> Context::createSurface()
{
	 return std::make_shared<Surface>(*this);
}

} // ns Graphics
} // ns Lor