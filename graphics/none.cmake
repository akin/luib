cmake_minimum_required(VERSION 3.7)

set(LIBRARY_PATH "${CMAKE_CURRENT_LIST_DIR}/none")
include_directories("${LIBRARY_PATH}/inc")

list(APPEND GRAPHICS_SOURCES 
    "${LIBRARY_PATH}/inc/graphics_api.h"
    "${LIBRARY_PATH}/src/context.cpp"
    "${LIBRARY_PATH}/src/surface.cpp"
)
