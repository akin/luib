#pragma once
#ifndef CORE_COMMON_H_
#define CORE_COMMON_H_

#include <cstdint>
#include <string>
#include <glm/glm.hpp>
#include <json/json.hpp>

namespace Lor {
	using Vec2 = glm::vec2;
	using Vec2i = glm::ivec2;
	using Vec3 = glm::vec3;
	using Vec3i = glm::ivec3;
	using Vec4 = glm::vec4;
	using Vec4i = glm::ivec4;
	using Matrix = glm::mat4;
	using Quaternion = glm::quat;

	using KeyCode = uint32_t;

	using Json = nlohmann::json;

	enum class InputSource
	{
		none = 0,
		keyboard,
		mouse,
		touch,
		joystick,
		custom,
	};
} // ns Lor

#endif