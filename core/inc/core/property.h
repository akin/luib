#pragma once
#ifndef CORE_PROPERTY_H_
#define CORE_PROPERTY_H_

namespace Lor {

template <class CType>
class Property
{
private:
	CType *ptr = nullptr;
public:
	Property()
	{
	}
	
	virtual ~Property()
	{
	}
	
	void set(CType * val)
	{
		ptr = val;
	}
	
	// to solve this method, you might want to use:
	// class AA : public Property<Foo>; ... { Property<Foo>::get() }
	CType *get() const
	{
		return ptr;
	}
};

} // ns Lor

#endif // CORE_PROPERTY_H_