#pragma once
#ifndef CORE_EVENT_H_
#define CORE_EVENT_H_

#include <cstdint>
#include <core/common.h>

namespace Lor {

enum class ButtonState : uint8_t {
	none = 0,
	down = 1,
	up = 2
};
enum class ActiveState : uint8_t {
	none = 0,
	down = 1,
	up = 2
};

class Event
{
public:
};

class ResizeEvent : public Event
{
public:
	Vec2i size;
};

class ResizeListener
{
public:
	virtual ~ResizeListener() {}
	virtual bool handle(const Lor::ResizeEvent& event) = 0;
};

} // ns Lor

#endif // CORE_EVENT_H_