#pragma once
#ifndef CORE_CONFIG_H_
#define CORE_CONFIG_H_

#include "common.h"
#include <json/json.hpp>

namespace Lor {
	using Config = nlohmann::json;
} // ns Lor

#endif // CORE_EVENT_H_