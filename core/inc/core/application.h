#pragma once
#ifndef CORE_APPLICATION_H_
#define CORE_APPLICATION_H_

#include "common.h"

namespace Lor {

class Application
{
public:
	Application();
	virtual ~Application();

	virtual bool init();
	virtual int run();
	virtual void clean();

	virtual void quit(bool success);
};
} // ns Lor

#endif // CORE_APPLICATION_H_