#pragma once
#ifndef CORE_OS_H_
#define CORE_OS_H_

#include <core/common.h>

#ifdef PLATFORM_WINDOWS
	#define NOMINMAX
	#include <windows.h>

	using SurfaceHandle = HWND;

	#if defined(EXPORTS)
		#define  LOR_EXPORT __declspec(dllexport)
	#else
		#define  LOR_EXPORT __declspec(dllimport)
	#endif
#endif

#ifdef PLATFORM_ANDROID
	using SurfaceHandle = EGLSurface;
#endif

#ifdef PLATFORM_LINUX
	using SurfaceHandle = Window;
#endif

#ifdef PLATFORM_MAC
	using SurfaceHandle = NSWindow * ;
#endif

#ifdef PLATFORM_MIR
	using SurfaceHandle = MirSurface * ;
#endif

#ifdef PLATFORM_VIVANTE
	using SurfaceHandle = EGLNativeWindowType;
#endif

#ifdef PLATFORM_NONE
	using SurfaceHandle = void*;
#endif


#ifndef LOR_EXPORT
	#define LOR_EXPORT
#endif

#endif