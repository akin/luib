#pragma once
#ifndef LOR_MOUSE_H_
#define LOR_MOUSE_H_

#include <core/event.h>
#include <core/common.h>
#include <functional>
#include <core/property.h>

namespace Lor {
class MouseMoveEvent;
class MouseClickEvent;
class MouseWheelEvent;
class MouseListener;

class Mouse : public Property<MouseListener>
{
public:
	enum class Button : uint8_t
	{
		none = 0,
		left = 1,
		right = 2,
		middle = 3,
		extra1,
		extra2,
		extra3,
		extra4,
	};
public:
	virtual ~Mouse(){}
};

class MouseListener
{
public:
	virtual ~MouseListener() {}

	virtual bool handle(const Lor::MouseMoveEvent& event) = 0;
	virtual bool handle(const Lor::MouseClickEvent& event) = 0;
	virtual bool handle(const Lor::MouseWheelEvent& event) = 0;
};
class MouseMoveEvent : public Event
{
public:
	uint32_t id;
	Vec2i position;

	Vec2i relative;
};
class MouseClickEvent : public Event
{
public:
	uint32_t id;
	Vec2i position;

	ButtonState state;
	Mouse::Button button = Mouse::Button::none;
	uint8_t clicks = 0;
};
class MouseWheelEvent : public Event
{
public:
	uint32_t id;
	Vec2i position;
};

} // ns Lor

#endif // LOR_MOUSE_H_