#pragma once
#ifndef LOR_SYSTEM_CONTROLLER_H_
#define LOR_SYSTEM_CONTROLLER_H_

namespace Lor {
class SystemController
{
public:
	class Settings 
	{
	public:
	};
public:
	virtual ~SystemController() {}

	virtual bool init(Settings settings) = 0;
	virtual void quit() = 0;
	virtual void update() = 0;

	virtual Settings getSettings() = 0;
};
} // ns Lor

#endif // LOR_SYSTEM_CONTROLLER_H_