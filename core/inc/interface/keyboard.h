#pragma once
#ifndef LOR_KEYBOARD_H_
#define LOR_KEYBOARD_H_

#include <core/event.h>
#include <core/property.h>
#include <platform/common.h>
#include <functional>

namespace Lor {
class KeyEvent;
class KeyboardListener;
class Keyboard : public Property<KeyboardListener>
{
public:
	virtual ~Keyboard() {}

	virtual std::string getKey(KeyCode code) = 0;
	virtual KeyCode getKey(std::string code) = 0;
};

class KeyboardListener
{
public:
	virtual ~KeyboardListener() {}
	virtual bool handle(const Lor::KeyEvent& event) = 0;
};
class KeyEvent : public Event
{
public:
	KeyCode code;
	ButtonState state;
};

} // ns Lor

#endif // LOR_KEYBOARD_H_