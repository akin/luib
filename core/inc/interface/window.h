#pragma once
#ifndef LOR_WINDOW_H_
#define LOR_WINDOW_H_

#include <core/os.h>
#include <core/property.h>
#include <core/event.h>

#include <vector>
#include <string>

#ifndef APPLICATION_NAME
	#define APPLICATION_NAME "Test project"
#endif

namespace Lor {
class WindowEvent;
class WindowListener;
class Window : public Property<WindowListener>, public Property<ResizeListener>
{
public:
	class Settings 
	{
	public:
		bool fullscreen = false;
		bool fullscreenDesktop = false;
		bool hidden = false;
		bool borderless = false;
		bool resizable = false;
		bool minimized = false;
		bool maximized = false;
		bool inputGrabbed = false;
		bool highDPI = false;
		Vec2i size{800, 600};
		std::string title{ APPLICATION_NAME };
	};
public:
	virtual ~Window() {}

	virtual void setSize(int width, int height) = 0;
	virtual void setTitle(std::string title) = 0;

	virtual int getWidth() const = 0;
	virtual int getHeight() const = 0;

	virtual std::string getTitle() const = 0;

	virtual SurfaceHandle getSurfaceHandle() = 0;

	virtual bool init(Settings settings) = 0;
	virtual void destroy() = 0;

	virtual Settings getSettings() = 0;
};

class WindowListener
{
public:
	virtual ~WindowListener() {}
	virtual bool handle(const Lor::WindowEvent& event) = 0;
};
class WindowEvent : public Event
{
public:
	enum class Type : uint32_t {
		none = 0,
		close,
		minimize,
		maximize,
		show,
		hide,
		move,
		restore,
		mouse_enter,
		mouse_leave,
		focus_gained,
		focus_lost
	};
	Type type;
};
} // ns Lor

#endif // LOR_WINDOW_H_