#include <core/application.h>
#include <iostream>

namespace Lor {

Application::Application()
{
}

Application::~Application()
{
	clean();
}

bool Application::init()
{
	return false;
}

int Application::run()
{
	return EXIT_FAILURE;
}

void Application::clean()
{
}

void Application::quit(bool success)
{
}
} // ns Lor