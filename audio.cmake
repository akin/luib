
set(AUDIO_NAME "SteamAudio")
set(AUDIO_VERSION "2.0-beta.9")

add_subdirectory("${PROJECT_LIBS_PATH}/steamaudio")

add_definitions(-DAPI_AUDIO="${API_AUDIO}")
add_definitions(-DAUDIO_VERSION="${AUDIO_VERSION}")