#pragma once
#ifndef PLATFORM_LOG_H_
#define PLATFORM_LOG_H_

#include <core/common.h>
#include <fmt/format.h>

namespace Lor {

class Log
{
private:
	static Log *singleton;
public:
	enum class Level : size_t {
		Message = 1,
		Warning = 16,
		Error = 32
	};
public:
	Log();
	~Log();

	void print(const char *file, size_t line, Level level, std::string message);
	void print(Level level, std::string message);

	static Log *getSingleton();
};

} // ns Lor

 // LOG Uses fmt style formatting
 // https://github.com/fmtlib/fmt
 // LOG_MESSAGE("Free the {}", "whales");
 // LOG_MESSAGE("Free the {0} {1}, {0}", "whales", "now");
#define LOG_MESSAGE(msg, ...) ::Lor::Log::getSingleton()->print(__FILE__, __LINE__, ::Lor::Log::Level::Message, fmt::format(msg,  __VA_ARGS__))
#define LOG_WARNING(msg, ...) ::Lor::Log::getSingleton()->print(__FILE__, __LINE__, ::Lor::Log::Level::Warning, fmt::format(msg,  __VA_ARGS__))
#define LOG_ERROR(msg, ...) ::Lor::Log::getSingleton()->print(__FILE__, __LINE__, ::Lor::Log::Level::Error, fmt::format(msg,  __VA_ARGS__))

#endif // PLATFORM_LOG_H_