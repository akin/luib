#include <platform/log.h>
#include <platform/common.h>
#include <cassert>
#include <iostream>
#include <cstdarg>

namespace Lor {

Log *Log::singleton = nullptr;

Log *Log::getSingleton()
{
	if (singleton == nullptr)
	{
		singleton = new Log;
	}
	return singleton;
}

Log::Log()
{
}

Log::~Log()
{
}

std::string toString(Log::Level level)
{
	if (level < Log::Level::Warning)
	{
		return "Log";
	}
	if (level < Log::Level::Error)
	{
		return "Warning";
	}
	return "Error";
}

void Log::print(const char *file, size_t line, Level level, std::string message)
{
	std::string out = fmt::format("{2} {0}:{1} - {3}\n", file, line, toString(level), message);
#ifdef PLATFORM_WINDOWS
	OutputDebugString(out.c_str());
#endif
	std::cout << out;
}

void Log::print(Level level, std::string message)
{
	std::string out = fmt::format("{0} : {1}\n", toString(level), message);
#ifdef PLATFORM_WINDOWS
	OutputDebugString(out.c_str());
#endif
	std::cout << out;
}

} // ns Lor
