cmake_minimum_required(VERSION 3.7)
cmake_policy(VERSION 3.7)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

list(APPEND BUILD_ARCHITECTURES
    x86
    amd64
)
include("${CMAKE_SOURCE_DIR}/cmake/platform.cmake")

include("project.cmake")
include("${PROJECT_ROOT}/engine.cmake")

set(PROJECT_LIBRARIES)
set(PROJECT_DLL_LIST)
set(PROJECT_INCLUDE_DIRS)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_EXTENSIONS ON)

# we need to include the luib libraries, add_subdirectory is going to ruin the "unit" that we are trying to build.
# with its visibility rules (folders and stuff, the luib needs to see sibling libraries as well).
# -- lowest tier libraries
include("${PROJECT_ROOT}/core/CMakeLists.txt")
# --
include("${PROJECT_ROOT}/platform/CMakeLists.txt")
# --
include("${PROJECT_ROOT}/graphics/CMakeLists.txt")
include("${PROJECT_ROOT}/audio.cmake")
# --
include("${PROJECT_ROOT}/window/CMakeLists.txt")

list(APPEND PROJECT_LIBRARIES LorCore LorPlatform LorWindow LorGraphics)

project(${APPLICATION_NAME} C CXX)

include_directories(${PROJECT_INCLUDE_DIRS})

add_subdirectory("main")

set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${APPLICATION_NAME})
